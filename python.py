import os

def ret_time(line):

	line = line.strip(',').strip('s')
	if ':' in line:
		time = line.split(':')
		return  int(time[0]), int(time[1]) 
	else:
		return  int(line)//60, int(line)%60 
		
def word_count(song_lyrics):

    unique_words = set()
    for line in song_lyrics:
        words = line.split(' ')
        for word in words:
            unique_words.add(word.strip().strip(',').strip('.').lower())

    return len(unique_words)


dict_data = {}
for (dirpath, dirname, filename) in os.walk('./songs'):

	for one_file in filename:
		f = open(dirpath+'/'+one_file)
		data = f.readlines()
		#print(data[0].strip('\n') + " " +data[2].strip('\n').strip(','))
		
		band_name = data[0].strip('\n').lower().strip()
		song_name= data[1].strip('\n').lower().strip()
		min_, sec = ret_time(data[2].strip('\n').strip()) 
		w_count = word_count(data[3:])
		time =  "{} min {} sec ".format(min_, sec)
		
		if band_name not in dict_data:
			dict_data[band_name] = {}
		
		dict_data[band_name][song_name] = {'time': time,
                                'wordcnt': w_count
                                }
		
		
		f.close()

		
import pprint
import random

print ("Dictionary ready!")


def random_song(our_dict):
	return random.choice(list(our_dict[random.choice(list(our_dict.keys()))].keys()))


def to_file(our_dict, filename):
	import csv
	bands = list(our_dict.keys())
	with open(filename+'.csv','wb') as file:
		new_file_writer = csv.writer(file)
		new_file_writer = csv.writer(file)
		new_file_writer.writerow(['band name','song no','song title'])
		for i in bands:
			songnum = 1
			bandname = i
			for j in list(our_dict[i].keys()):
				new_file_writer.writerow([bandname,songnum,j])
				songnum+=1
				bandname = ''
	print ('\n'+'************************************************')
	print ('data succesfully saved in file ' + filename + '.csv')
	print ('************************************************')


csvfilename = raw_input('Enter output .csv file name:'+'\n')

to_file(dict_data, csvfilename)

print ('\n'+"Here's a random song title for you:"+'\n')
print (random_song(dict_data))
print ('\n'+"Isn't it a great song?")
